# README #

This repo contains code to learn linear decision boundaries using Kaczmarz's algorithm, based on the idea of alternating projections.
This was performed as part of the course on Vector Space Methods.
The bulk of the code is in altproj_code.py
The basic test cases are in altproj_code_test.py

The Notebook contains plots and solved exercises.

