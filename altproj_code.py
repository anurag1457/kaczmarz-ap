import numpy as np
from scipy.linalg import inv, svd
from tqdm import tqdm_notebook as tqdm
from scipy.optimize import linprog

### Helper functions

# Compute null space
def null_space(A, rcond=None):
    """
    Compute null spavce of matrix XProjection on half space defined by {v| <v,w> = c}
    Arguments:
        A {numpy.ndarray} -- matrix whose null space is desired
        rcond {float} -- intercept
    Returns:
        Q {numpy.ndarray} -- matrix whose (rows?) span null space of A
    """
    u, s, vh = svd(A, full_matrices=True)
    M, N = u.shape[0], vh.shape[1]
    if rcond is None:
        rcond = np.finfo(s.dtype).eps * max(M, N)
    tol = np.amax(s) * rcond
    num = np.sum(s > tol, dtype=int)
    Q = vh[num:,:].T.conj()
    return Q

### End Helper Functions

def get_proj(A):
    return A.dot(inv(A.T.dot(A))).dot(A.T)

# Exercise 1: Alternating projection for subspaces
def altproj(A, B, v0, n):
    """
    Arguments:
        A {numpy.ndarray} -- matrix whose columns form basis for subspace U
        B {numpy.ndarray} -- matrix whose columns form baiss for subspace W
        v0 {numpy.ndarray} -- initialization vector
        n {int} -- number of sweeps for alternating projection
    Returns:
        v {numpy.ndarray} -- the output after 2n steps of alternating projection
        err {numpy.ndarray} -- the error after each full pass
    """
    err = np.zeros((n,))

    projA= get_proj(A)
    projB= get_proj(B)
    ns=null_space(np.hstack([A,-B]))
    v_new=v0

    if ns.size>0:
        Uintw = np.matmul(np.hstack([A, B]), ns)
        Uintw_proj = get_proj(Uintw)
        v_best = Uintw_proj.dot(v_new)
    else:
        v_best = np.zeros_like(v_new)


    for m in range(2*n):
        curr = projA if m %2==0 else projB
        v_new = curr.dot(v_new)
        if m%2==0:
            err[int(m/2)] = np.max(np.abs(v_best - v_new))
    return v_new, err

# Exercise 2: Kaczmarz algorithm for solving linear systems
def kaczmarz(A, b, I):
    """
    Arguments:
        A {numpy.ndarray} -- matrix defines the LHS of linear equation
        b {numpy.ndarray} -- vector defines the RHS of linear equation
        I {int} -- number of full passes through the Kaczmarz algorithm
    Returns:
        X {numpy.ndarray} -- the output of all I full passes
        err {numpy.ndarray} -- the error after each full pass
    """
    
    ### Add code here
    A_rows = [A[i,:] for i in range(A.shape[0])]
    v = np.zeros((A.shape[1]))
    num_equations=A.shape[0]
    err = np.zeros((I,))
    X=np.zeros((A.shape[1],I))
    for i in range(I):
        for m in range(num_equations):
            norm=A_rows[m].dot(A_rows[m])

            v = v - (((v.dot(A_rows[m]) - b[m])/norm) * A_rows[m])
            err[i]=np.max(np.abs(A.dot(v)-b))
            X[:,i]=v
    return X, err



# Exercise 4: Alternating projection to satisfy linear inequalities
def lp_altproj(A, b, I, s=1):
    """
    Find a feasible solution for A v >= b using alternating projection
    with every entry of v0 obeying Uniform[0,1]
    Arguments:
        A {numpy.ndarray} -- matrix defines the LHS of linear equation
        b {numpy.ndarray} -- vector defines the RHS of linear equation
        I {int} -- number of full passes through the alternating projection
        s {numpy.float} -- step size of projection (defaults to 1)
    Returns:
        v {numpy.ndarray} -- the output after I full passes
        err {numpy.ndarray} -- the error after each full pass
    """
    
    # Add code here
    A_rows = [A[i, :] for i in range(A.shape[0])]
    v = np.zeros((A.shape[1]))
    num_equations = A.shape[0]
    err = np.zeros((I,))
    X = np.zeros((A.shape[1], I))
    # res = linprog(c, A_ub=-A, b_ub=-b, bounds=[(0, None)] * c.size, method='interior-point')
    # x_scipy, p_scipy = res.x, res.fun
    for i in range(I):
        for m in range(num_equations):
            norm = A_rows[m].dot(A_rows[m])
            if A_rows[m].dot(v) >= b[m]:
                continue
            v = v - ((v.dot(A_rows[m]) - b[m]) / norm) * A_rows[m]
            # v=np.maximum(np.zeros(v.shape), v)#CHANGE FOR MNIST
        err[i] = np.max(b - A.dot(v))
        X[:, i] = v
    return v, err


from sklearn.model_selection import train_test_split


def data_split(df, d, test_size=0.5):
    df_d = df.loc[df['label'] == d]  # extract digit d
    X = df_d['feature']
    y = df_d['label']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=0)
    X_train = X_train.values  # convert datafram to array
    X_test = X_test.values
    y_train = y_train.values
    y_test = y_test.values

    X_tr = np.zeros((X_train.shape[0], 784))
    y_tr = np.zeros(y_train.shape[0])
    for i in range(X_train.shape[0]):
        for j in range(784):
            X_tr[i, j] = X_train[i][j]
    for i in range(y_train.shape[0]):
        y_tr[i] = y_train[i]
    X_tr = np.insert(X_tr, 784, -1, axis=1)

    X_te = np.zeros((X_test.shape[0], 784))
    y_te = np.zeros(y_test.shape[0])
    for i in range(X_test.shape[0]):
        for j in range(784):
            X_te[i, j] = X_test[i][j]
    for i in range(y_test.shape[0]):
        y_te[i] = y_test[i]
    X_te = np.insert(X_te, 784, -1, axis=1)
    return X_tr, X_te, y_tr, y_te




if __name__ == "__main__":
    c = np.array([3, -1, 2])
    A = np.array([[2, -1, 1], [1, 0, 2], [-7, 4, -6]])
    b = np.array([-1, 2, 1])


    I = 500
    n = A.shape[0]
    # Do not forget constraint xi >= 0

    A1 = np.vstack((A, -c))
    b1 = np.zeros((4,))

    b1[0:n] = b

    print(A1)
    print(b1)

    x, err_useless = lp_altproj(A1, b1, I)