import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from numpy.random import rand, randn
from scipy.linalg import inv, svd
from scipy.optimize import linprog

from altproj_code import null_space, altproj, lp_altproj, kaczmarz, data_split

def mnist_pairwise_altproj(df, a, b, solver, test_size=0.1, verbose=False):

    Xa_tr, Xa_te, ya_tr, ya_te = data_split(df, a, test_size)
    Xb_tr, Xb_te, yb_tr, yb_te = data_split(df, b, test_size)
    for i in range(ya_tr.shape[0]):
        ya_tr[i] = -1
    for i in range(ya_te.shape[0]):
        ya_te[i] = -1
    for i in range(yb_tr.shape[0]):
        yb_tr[i] = 1
    for i in range(yb_te.shape[0]):
        yb_te[i] = 1
    X_tr = np.concatenate((Xa_tr, Xb_tr), axis=0)
    X_te = np.concatenate((Xa_te, Xb_te), axis=0)
    y_te = np.concatenate((ya_te, yb_te), axis=0)
    y_tr = np.concatenate((ya_tr, yb_tr), axis=0)
    A_star = np.multiply(X_tr, y_tr[:, np.newaxis])
    z_hat, err = solver(A_star, np.ones(y_tr.shape[0]))

    # Compute estimation and misclassification on training set
    y_hat_tr = X_tr.dot(z_hat)
    for i in range(y_hat_tr.shape[0]):
        if y_hat_tr[i] >= 0:
            y_hat_tr[i] = 1
        else:
            y_hat_tr[i] = -1

    cm_tr = confmat(y_tr, y_hat_tr, 2)

    # for i in range(y_hat_tr.shape[0]):
    #     if (y_tr[i] == -1 and y_hat_tr[i] == -1):
    #         cm_tr[0, 0] = cm_tr[0, 0] + 1
    #     elif (y_tr[i] == -1 and y_hat_tr[i] == 1):
    #         cm_tr[0, 1] = cm_tr[0, 1] + 1
    #     elif (y_tr[i] == 1 and y_hat_tr[i] == -1):
    #         cm_tr[1, 0] = cm_tr[1, 0] + 1
    #     elif (y_tr[i] == 1 and y_hat_tr[i] == 1):
    #         cm_tr[1, 1] = cm_tr[1, 1] + 1

    err_tr = (cm_tr[0, 1] + cm_tr[1, 0]) / y_hat_tr.shape[0]

    # Compute estimation and misclassification on testing set
    y_hat_te = X_te.dot(z_hat)
    for i in range(y_hat_te.shape[0]):
        if y_hat_te[i] >= 0:
            y_hat_te[i] = 1
        else:
            y_hat_te[i] = -1

    cm_te = confmat(y_te, y_hat_te, 2)

    # for i in range(y_hat_te.shape[0]):
    #     if (y_te[i] == -1 and y_hat_te[i] == -1):
    #         cm_te[0, 0] = cm_te[0, 0] + 1
    #     elif (y_te[i] == -1 and y_hat_te[i] == 1):
    #         cm_te[0, 1] = cm_te[0, 1] + 1
    #     elif (y_te[i] == 1 and y_hat_te[i] == -1):
    #         cm_te[1, 0] = cm_te[1, 0] + 1
    #     elif (y_te[i] == 1 and y_hat_te[i] == 1):
    #         cm_te[1, 1] = cm_te[1, 1] + 1

    err_te = (cm_te[0, 1] + cm_te[1, 0]) / y_hat_te.shape[0]

    if verbose:
        print('Pairwise experiment, mapping {0} to -1, mapping {1} to 1'.format(a, b))
        print('training error = {0:.2f}%, testing error = {1:.2f}%'.format(100 * err_tr, 100 * err_te))

        # Compute confusion matrix for training set
        # cm_tr =
        print('Training set confusion matrix:\n {0}'.format(cm_tr))

        # Compute confusion matrix for testing set
        # cm_te =
        print('Testing set confusion matrix:\n {0}'.format(cm_te))

        # Compute the histogram of the function output separately for each class
        # Then plot the two histograms together
        ya_te_hat = Xa_te.dot(z_hat)
        yb_te_hat = Xb_te.dot(z_hat)
        output = [ya_te_hat, yb_te_hat]
        # plt.figure(figsize=(8, 4))
        # plt.hist(output, bins=50)

    res = np.array([err_tr, err_te])
    return z_hat, res



def mnist_multiclass_altproj(df, solver, test_size=0.5):
    indices = np.arange(len(df))
    np.random.shuffle(indices)
    df = df.iloc[indices]
    trainset, testset = df.iloc[0:int(test_size * len(df))], df.iloc[int(test_size * len(df)):len(df)]
    X_tr = np.stack(trainset.values[:, 0], axis=0)
    X_tr = np.append(X_tr, -1 * np.ones((X_tr.shape[0], 1)), 1)
    X_te = np.stack(testset.values[:, 0], axis=0)
    X_te = np.append(X_te, -1 * np.ones((X_te.shape[0], 1)), 1)
    y_tr = np.array(trainset.values[:, 1])
    y_te = np.array(testset.values[:, 1])
    # Run solver on training set to get linear classifier
    A_hat = np.zeros((X_tr.shape[0] * 9, X_tr.shape[1] * 10))
    for i in range(X_tr.shape[0]):
        for j in range(10):
            if j < y_tr[i]:
                A_hat[9 * i + j, y_tr[i] * 785:(y_tr[i] + 1) * 785] = X_tr[i]
                A_hat[9 * i + j, j * 785:(j + 1) * 785] = -1 * X_tr[i]
            if j > y_tr[i]:
                A_hat[9 * i + j - 1, y_tr[i] * 785:(y_tr[i] + 1) * 785] = X_tr[i]
                A_hat[9 * i + j - 1, (j) * 785:(j + 1) * 785] = -1 * X_tr[i]
    b_tilde = np.zeros(len(A_hat))
    Z, err = solver(A_hat, b_tilde)
    Z = np.reshape(Z, (10, 785))
    Z = Z.T
    y_hat_tr = X_tr @ Z
    err_train = np.sum(np.array([np.argmax(y_hat_tr, axis=1) != y_tr])) / len(y_tr)
    y_hat_te = X_te @ Z
    err_test = np.sum(np.array([np.argmax(y_hat_te, axis=1) != y_te])) / len(y_te)
    print('training error = {0:.2f}%, testing error = {1:.2f}%'.format(100 * err_train, 100 * err_test))
    # Compute confusion matrix for training set
    confmat_train = confmat(y_tr.astype(int), np.argmax(y_hat_tr, axis=1).astype(int), 10)
    print('Training set confusion matrix:\n {0}'.format(confmat_train))
    # Compute confusion matrix for testing set
    confmat_test = confmat(y_te.astype(int), np.argmax(y_hat_te, axis=1).astype(int), 10)
    print('Testing set confusion matrix:\n {0}'.format(confmat_test))
    res = np.array([err_train, err_test])
    return Z, res

def confmat(y, yhat, num_classes):
    cm = np.zeros((num_classes, num_classes))
    for i in range(y.shape[0]):
        a= int(y[i])
        b= int(yhat[i])
        try:
            cm[a,b]+=1
        except IndexError:
            print(a)
            print(type(a))
            print(b)
            print(type(b))

    return cm


pdf = pd.read_csv('mnist_train.csv')
# append feature column by merging all pixel columns
pdf['feature'] = pdf.apply(lambda row: row.values[1:], axis=1)
# only keep feature and label column
pdf = pdf[['feature', 'label']]
pdf.head()

# solver = lambda A, b: lp_altproj(A, b + 1e-6, 1)
# Z, res = mnist_multiclass_altproj(pdf, solver)



solver = lambda A, b: lp_altproj(A, b + 1e-6, 100, 1)
z_hat, res = mnist_pairwise_altproj(pdf, 2, 3, solver, verbose=True)

